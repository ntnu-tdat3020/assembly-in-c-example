# Hello World in C

## Prerequisites
  * Linux or MacOS
  * The C++ IDE [juCi++](https://github.com/cppit/jucipp) should be installed.

## Installing dependencies

## Compiling and running
In a terminal:
```sh
git clone https://gitlab.com/ntnu-tdat3020/assembly-in-c-example
juci assembly-in-c-example
```

Choose Compile and Run in the Project menu.
