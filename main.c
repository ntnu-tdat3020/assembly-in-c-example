//Based on http://www.ibiblio.org/gferg/ldp/GCC-Inline-Assembly-HOWTO.html#s5
#include <stdio.h>

int main() {
  size_t a = 10, b;

  // b = a + 5 :
  asm("mov rax, %1;" // The value of a is copied to the eax register
      "add rax, 5;"  // Add 5 to the eax register
      "mov %0, rax;" // Copy the value of the eax register to b
      : "=r"(b)      // Output (%0)
      : "r"(a)       // Input (%1)
      : "rax");      // Clobbered register (GCC will not overwrite this register)

  printf("b = %ld\n", b);
}
